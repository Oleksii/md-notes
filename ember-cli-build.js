/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var mergeTrees = require('broccoli-merge-trees');
var pickFiles = require('broccoli-static-compiler');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here
  });
  app.import("bower_components/ember-localstorage-adapter/localstorage_adapter.js");
  app.import("bower_components/moment/min/moment-with-locales.min.js");
  // app.import("bower_components/showdown/dist/showdown.min.js");
  // app.import("bower_components/showdown-github/dist/showdown-github.min.js");
  // app.import("bower_components/showdown-table/dist/showdown-table.min.js");
  // app.import("bower_components/markdown/lib/markdown.js");
  app.import("bower_components/marked/lib/marked.js");
  var fontAwesome = pickFiles('bower_components/font-awesome/fonts', {
      srcDir: '/',
      files: ['**/*'],
      destDir: '/fonts'
  })
  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return mergeTrees([app.toTree(), fontAwesome]);
};
