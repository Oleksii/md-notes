import Ember from 'ember';
import EditorMixinMixin from '../../../mixins/editor-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | editor mixin');

// Replace this with your real tests.
test('it works', function(assert) {
  var EditorMixinObject = Ember.Object.extend(EditorMixinMixin);
  var subject = EditorMixinObject.create();
  assert.ok(subject);
});
