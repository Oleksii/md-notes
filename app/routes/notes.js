import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        return this.store.findAll('note');
    },
    setupController(controller, model) {
        controller.set('model', model);
    }
});
