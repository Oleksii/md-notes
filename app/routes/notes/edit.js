import Ember from 'ember';

export default Ember.Route.extend({
    renderTemplate() {
        this.render('notes/edit', {
            into: 'application'
        });
    },
    model(params) {
        return this.store.findRecord('note', params.id);
    },
    actions: {
        willTransition(transition) {
            let model = this.modelFor('notes/edit');
            if(transition.targetName === 'notes.index' && model.get('hasDirtyAttributes')){
                var wantLeave = confirm('Изменения не сохранены. Хотите уйти?');
                if(wantLeave){
                    model.rollbackAttributes();
                }else{
                    transition.abort();
                }
            }
        }
    }
});
