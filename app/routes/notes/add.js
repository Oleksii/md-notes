import Ember from 'ember';

export default Ember.Route.extend({
    renderTemplate() {
        this.render('notes/add', {
            into: 'application'
        });
    },
    model() {
        return this.store.createRecord('note');
    },
    setupController(controller,model){
        if(!controller.get('model')){
            controller.set('model',model)
        }else if(!controller.get('model.isNew')){
            controller.set('model',model)
        }
    },
    actions: {
        willTransition(transition) {
            let model = this.modelFor('notes/add');
            if(transition.targetName == 'notes.index' && model.get('isNew') && model.get('content')){
                var wantSave = confirm('Хотите сохранить новую запись?');
                if(wantSave){
                    this.get('controller').send('save');
                }else{
                    model.destroyRecord();
                }
            }
        }
    }
});
