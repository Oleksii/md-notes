import Ember from 'ember';

export default Ember.Route.extend({
    renderTemplate() {
        this.render('notes/view', {
            into: 'application'
        });
    },
    model(params) {
        return this.store.findRecord('note', params.id);
    },
});
