import Ember from 'ember';

export default Ember.Mixin.create({
    mdTextarea: null,
    historyUndo: Ember.A([]),
    historyRedo: Ember.A([]),
    disabledUndo: Ember.computed('model.content', function(){
        return this.get('historyUndo.length') === 0;
    }),
    disabledRedo: Ember.computed('model.content', function(){
        return this.get('historyRedo.length') === 0;
    }),
    dontPush: true,
    historyObserve: Ember.observer('model.content', function(){
        if(this.get('dontPush') || this.get('model.content')===undefined)
            return this.set('dontPush', false);
        this.get('historyUndo').unshift(this.get('model.content'));
        if(this.get('historyUndo.length')>10)
            this.get('historyUndo').pop();
    }),
    isEditing: false,
    actions: {
        save() {
            this.model.save();
        },
        remove() {
            if (confirm('Действительно хотите удалить запись?')) {
                this.model.destroyRecord().then(() => {
                    this.transitionToRoute('notes');
                })
            }
        },
        goBack() {
            window.history.back();
        },
        undo() {
            this._someDo(this.get('historyUndo'),this.get('historyRedo'))
        },
        redo() {
            this._someDo(this.get('historyRedo'),this.get('historyUndo'))
        }
    },
    _someDo(historyFrom, historyTo) {
        let mdCore = this.get('mdTextarea');
        if(historyFrom.get('length')){
            let selectionStart = mdCore.get('element.selectionStart');
            mdCore.set('realSelectionStart', selectionStart);
            mdCore.set('realSelectionEnd', selectionStart);
            let newModel;
            do {
                newModel = historyFrom.shift();
                historyTo.unshift(newModel);
            } while(newModel===this.get('model.content'))
            this.set('dontPush', true);
            mdCore.send('updateValue', newModel);
        } else {
            mdCore.$().focus();
        }
    }
});
