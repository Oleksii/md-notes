import Ember from 'ember';

export function mdShorten(params /*, hash*/ ) {
    if(params[0]){
        let length = params[1] || 50;
        let result = params[0].substr(0, length);
        return new Ember.Handlebars.SafeString(result);
    }
}

export default Ember.Helper.helper(mdShorten);
