import Ember from 'ember';

export function mdMoment(params /*, hash*/ ) {
    // return moment.unix(params[0]).format('DD-MM-YY HH:mm:ss');
    moment.locale('ru');
    return moment.unix(params[0]).format('LLLL');
}

export default Ember.Helper.helper(mdMoment);
