import Ember from 'ember';

let myTimer;

export default Ember.Controller.extend(Ember.SortableMixin, {
    sortProps: ['date:desc'],
    sortedModel: Ember.computed.sort('model', 'sortProps'),
    editMode: false,
    willTransitionParam: null,
    actions: {
        addNew() {
            this.transitionToRoute('notes.add');
        },
        startedTouch(param, e) {
            e.preventDefault();
            this.set('willTransitionParam', param);
            myTimer = setTimeout(function(){
                alert('olol');
                this.set('editMode', true);
            },1500);
        },
        endedTouch(e) {
            if(this.get('editMode')===false){
                clearTimeout(myTimer);
                this.transitionToRoute('notes.edit', this.get('willTransitionParam'));
            }
        }
    }
});
