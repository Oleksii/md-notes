import Ember from 'ember';
import Mixin from '../../mixins/editor-mixin';

export default Ember.Controller.extend(Mixin,{
    disabledView: Ember.computed('model.content', function(){
        console.log(this.get('model.content'),(!this.get('model.content')));
        return !this.get('model.content');
    }),
    actions: {
        save() {
            if(Ember.isEmpty(this.get('model.title'))){
                var title = prompt('Название записи:');
                this.model.set('title', title);
            }
            this.model.set('date', (new Date().getTime())/1000);
            this._super(...arguments);
            // this.model.save();
        },

        view() {
            if(this.get('model.content'))
                this.transitionToRoute('notes.view', this.get('model.id'));
        },
    }
});
