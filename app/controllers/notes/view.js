import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        topdf() {
            var name = this.get('model.id');
            var convertedHtml = this.get('convertedHtml');
            Ember.$.ajax({
                url: 'http://convertpdf.reclamare.ua/',
                type: 'post',
                data: {html: convertedHtml},
                success(response) {
                    var fileTransfer = new FileTransfer();
                    var uri = response;
                    var fileURI = ''+cordova.file.externalDataDirectory+name+'.pdf';
                    fileTransfer.download(
                        uri,
                        fileURI,
                        function(entry) {
                            cordova.plugins.fileOpener2.open(
                                entry.toURL(),
                                'application/pdf',
                                {
                                    error : function(){ },
                                    success : function(){
                                     }
                                }
                            );
                        },
                        function(error) {
                            alert("connection error");
                        },
                        true
                    );
                },
                error(err){
                    alert("connection error");
                }
            });


        }
    }
});
