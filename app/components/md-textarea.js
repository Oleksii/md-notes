import Ember from 'ember';

export default Ember.TextArea.extend({
    realSelectionStart: Ember.computed.oneWay('element.selectionStart'),
    realSelectionEnd: Ember.computed.oneWay('element.selectionEnd'),
    willInsertElement(){
        this.set('register-as', this);
        this.$().focus(() => {
            this.set('isEditing',true)
        });
        this.$().focusout(() => {
            let lt = setTimeout(() => {
                if(!this.$().is(":focus"))
                    this.set('isEditing',false);
            },50);
        });
    },
    isEditing: false,
    isDestroying() {
        this.$().off('focus', () => {
            this.set('isEditing',true)
        });
        this.$().off('focusout', () => {
            let lt = setTimeout(() => {
                if(!this.$().is(":focus"))
                    this.set('isEditing',false);
            },50);
        });
    },
    actions: {
        updateValue(value) {
            this.set('value', value);
            let st = setTimeout(()=>{
                this.set('element.selectionStart', this.get('realSelectionStart'));
                this.set('element.selectionEnd', this.get('realSelectionEnd'));
                this.$().focus();
            },50);
        }
    }
});
