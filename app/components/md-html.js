import Ember from 'ember';

export default Ember.Component.extend({
    convertedHtml: null,
    willInsertElement() {
        this.set('convertedHtml', marked(this.get('value')));
    }
});
