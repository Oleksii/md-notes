import Ember from 'ember';

export default Ember.Component.extend({
    _wrap(text) {
        let mdCore = this.get('mdCore');
        let oldContent = mdCore.get('value');
        let selectionStart = mdCore.get('element.selectionStart');
        let selectionEnd = mdCore.get('element.selectionEnd');
        mdCore.set('realSelectionStart', selectionStart + text.length);
        mdCore.set('realSelectionEnd', selectionEnd + text.length);
        mdCore.send('updateValue', [oldContent.substr(0, selectionStart),oldContent.substr(selectionStart, selectionEnd-selectionStart),oldContent.substr(selectionEnd)].join(text));
    },
    actions: {
        bold() {
            this._wrap('**');
        },
        italic() {
            this._wrap('_');
        },
        strike() {
            this._wrap('~');
        },
        list() {

            let listBlueprint = `
- item 1;
- item 2;
- item 3;
`;
            this.send('insertBlueprint',listBlueprint);
        },
        code() {

            let listBlueprint = `
\`\`\`
`;
            this._wrap(listBlueprint);
        },
        table() {

            let listBlueprint = `
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| **col 3 is**  | right-aligned | $1600 |
`;
            this.send('insertBlueprint',listBlueprint);
        },
        insertBlueprint(listBlueprint) {
            var mdCore = this.get('mdCore');
            let oldContent = mdCore.get('value');
            let selectionStart = mdCore.get('element.selectionStart');
            let selectionEnd = mdCore.get('element.selectionEnd');
            let value;
            if(selectionStart != selectionEnd){
                let ifBlueprint = oldContent.substr(selectionStart, selectionEnd-selectionStart);
                if(ifBlueprint === listBlueprint){
                    value = oldContent.substr(0, selectionStart) + oldContent.substr(selectionEnd);
                    selectionEnd = selectionStart;
                }else{
                    value = oldContent.substr(0, selectionStart) + listBlueprint +oldContent.substr(selectionEnd);
                    selectionEnd = selectionStart + listBlueprint.length;
                }
            }else{
                value = oldContent.substr(0, selectionStart) + listBlueprint +oldContent.substr(selectionEnd);
                selectionEnd = selectionStart + Math.ceil(listBlueprint.length/2);
                selectionStart = selectionEnd;
            }
            mdCore.set('realSelectionStart', selectionStart);
            mdCore.set('realSelectionEnd', selectionEnd);
            mdCore.send('updateValue', (''+value));
        }
    }
});
