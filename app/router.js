import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('notes', {path: '/'}, function() {
    this.route('edit', {path: '/edit/:id'});
    this.route('view', {path: '/view/:id'});
    this.route('add');
  });
});

export default Router;
